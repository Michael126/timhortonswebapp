package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

import sheridan.MealType;
import sheridan.MealsService;
import java.util.ArrayList;
import java.util.List;
public class MealsServiceTest {

	@Test
	public void testDrinksRegular() {
		assertTrue ("The Drinks are empty ", MealType.DRINKS != null);

	}
	@Test
	public void testDrinksException() {
		MealsService type = new MealsService();
		assertTrue ("the drink you selected is null ", type.getAvailableMealTypes(MealType.DRINKS).get(0) != null);
	}
	@Test
	public void testDrinksBoundaryIn() {
		MealsService type = new MealsService();
		assertTrue("There are more than 3 drinks ", type.getAvailableMealTypes(MealType.DRINKS).size() > 3);
	}
	@Test
	public void testDrinksBoundaryOut() {
		MealsService type = new MealsService();
		assertFalse("There are no Drinks ", type.getAvailableMealTypes(MealType.DRINKS).size() < 1 && type.getAvailableMealTypes(MealType.DRINKS) == null);
	}

}
